import socket

HOST = socket.gethostname()  # Endereco IP do Servidor
PORT = 5000  # Porta que o Servidor está

# Criando a conexão
tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
destino = (HOST, PORT)
tcp.connect(destino)

print('\nDigite suas mensagens')

# Recebendo a mensagem do usuário final pelo teclado
mensagem = ""

# Enviando a mensagem para o Servidor TCP através da conexão
mensagem = input()
tcp.send(str(mensagem).encode())
saida = tcp.recv(1024)
print("\n-----------------------------------------------------------------\n "
      "%s "
      "\n-----------------------------------------------------------------\n" % saida.decode('utf-8'))

# Fechando o Socket
tcp.close()